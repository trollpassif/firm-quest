using UnrealBuildTool;

public class FirmTarget : TargetRules
{
	public FirmTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Firm");
	}
}
