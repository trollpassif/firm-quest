# Firm Quest

Supervision du développement.

Développement de ces éléments :
  
  
[1.](https://gitlab.com/trollpassif/firm-quest/tree/develop/Content/TopDownBP/Trigger)  
[2.](https://gitlab.com/trollpassif/firm-quest/blob/develop/Content/TopDownBP/Widgets/widgetTypeService.uasset)  
[3.](https://gitlab.com/trollpassif/firm-quest/tree/develop/Content/TopDownBP/Dialogues)  
[4.](https://gitlab.com/trollpassif/firm-quest/tree/develop/Content/TopDownBP/AnimationConstruction)    
[5.](https://gitlab.com/trollpassif/firm-quest/tree/develop/Content/TopDownBP/Blueprints)
